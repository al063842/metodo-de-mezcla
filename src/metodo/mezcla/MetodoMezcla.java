
package metodo.mezcla;

/**
 *
 * @author ameri
 */
public class MetodoMezcla {

    
    public static void main(String[] args) {
        OrdenamientoMezcla ordenar = new OrdenamientoMezcla();
        int arre[] = {3, 4, 5, 1, 2, 6, 13, 22, 56};
        System.out.println("Arreglo inicial");
        ordenar.mostrarArreglo(arre);
        System.out.println("Arreglo ordenado por el metodo de mezcla");
        ordenar.mezclaFusion(arre);
        ordenar.mostrarArreglo(arre);
    }
}
