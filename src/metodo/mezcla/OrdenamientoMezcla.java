
package metodo.mezcla;

/**
 *
 * @author ameri
 */
public class OrdenamientoMezcla {

    //Metodo mezcla fusion
    public void mezclaFusion(int arreglo[]) {
        int izquierda = 0, izq = 0, derecha = arreglo.length - 1, der = derecha;
        boolean ordenado = false;
        do {
            ordenado = true;
            izquierda = 0;
            while (izquierda < derecha) {
                izq = izquierda;
                while (izq < derecha && arreglo[izq] <= arreglo[izq + 1]) {
                    izq++;
                }
                der = izq + 1;
                while (der == derecha - 1 || der < derecha && arreglo[der] <= arreglo[der + 1]) {
                    der++;
                }
                if (der <= derecha) {
                    mezcla(arreglo);
                    ordenado = false;
                }
                izquierda = izq;
            }
        } while (!ordenado);
    }
    //Metodo de mezlca   

    public void mezcla(int[] arreglo) {
        int i, j, k;
        if (arreglo.length > 1) {
            int nIzq = arreglo.length / 2;
            int nDer = arreglo.length - nIzq;
            int aIzq[] = new int[nIzq];
            int aDer[] = new int[nDer];

            //Copiar los datos de la primera parte al arreglo izquierdo
            for (i = 0; i < nIzq; i++) {
                aIzq[i] = arreglo[i];
            }
            //Copiar los datos de la segunda parte al arreglo derecho

            for (i = nIzq; i < nIzq + nDer; i++) {
                aDer[i - nIzq] = arreglo[i];
            }

            //recursividad
            i = 0;
            j = 0;
            k = 0;

            while (aIzq.length != j && aDer.length != k) {
                if (aIzq[j] < aDer[k]) {
                    arreglo[i] = aIzq[j];
                    i++;
                    j++;
                } else {
                    arreglo[i] = aDer[k];
                    i++;
                    k++;

                }
            }
            //Arreglo final

            while (aIzq.length != j) {
                arreglo[i] = aIzq[j];
                i++;
                j++;
            }

            while (aDer.length != k) {
                arreglo[i] = aDer[k];
                i++;
                k++;
            }
        }
    }

    public void mostrarArreglo(int[] arreglo) {
        int k;
        for (k = 0; k < arreglo.length; k++) {
            System.out.print("[" + arreglo[k] + "]");
        }
        System.out.println();
    }
}